##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x64
ProjectName            :=room-paths-scroll
ConfigurationName      :=Debug_x64
WorkspacePath          :=C:/Work/Dev/orx-projects/room-paths-scroll/build/windows/codelite
ProjectPath            :=C:/Work/Dev/orx-projects/room-paths-scroll/build/windows/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=03/12/2022
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/room-paths-scrolld.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="room-paths-scroll.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/ar.exe rcu
CXX      := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
CC       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/gcc.exe
CXXFLAGS :=  -g -ffast-math -m64 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -m64 $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
AR:=x86_64-w64-mingw32-gcc-ar
Objects0=$(IntermediateDirectory)/up_up_up_src_cell.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Tile.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Room.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Character.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c copy /Y C:\Work\Dev\orx\code\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_cell.cpp$(ObjectSuffix): ../../../src/cell.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_cell.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_cell.cpp$(DependSuffix) -MM ../../../src/cell.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/room-paths-scroll/src/cell.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_cell.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_cell.cpp$(PreprocessSuffix): ../../../src/cell.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_cell.cpp$(PreprocessSuffix) ../../../src/cell.cpp

$(IntermediateDirectory)/up_up_up_src_Tile.cpp$(ObjectSuffix): ../../../src/Tile.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Tile.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Tile.cpp$(DependSuffix) -MM ../../../src/Tile.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/room-paths-scroll/src/Tile.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Tile.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Tile.cpp$(PreprocessSuffix): ../../../src/Tile.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Tile.cpp$(PreprocessSuffix) ../../../src/Tile.cpp

$(IntermediateDirectory)/up_up_up_src_Room.cpp$(ObjectSuffix): ../../../src/Room.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Room.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Room.cpp$(DependSuffix) -MM ../../../src/Room.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/room-paths-scroll/src/Room.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Room.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Room.cpp$(PreprocessSuffix): ../../../src/Room.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Room.cpp$(PreprocessSuffix) ../../../src/Room.cpp

$(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(ObjectSuffix): ../../../src/room-paths-scroll.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(DependSuffix) -MM ../../../src/room-paths-scroll.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/room-paths-scroll/src/room-paths-scroll.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(PreprocessSuffix): ../../../src/room-paths-scroll.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_room-paths-scroll.cpp$(PreprocessSuffix) ../../../src/room-paths-scroll.cpp

$(IntermediateDirectory)/up_up_up_src_Character.cpp$(ObjectSuffix): ../../../src/Character.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Character.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Character.cpp$(DependSuffix) -MM ../../../src/Character.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/room-paths-scroll/src/Character.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Character.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Character.cpp$(PreprocessSuffix): ../../../src/Character.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Character.cpp$(PreprocessSuffix) ../../../src/Character.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


