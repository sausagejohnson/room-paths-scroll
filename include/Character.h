
#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include "room.h"
#include "room-paths-scroll.h"

#define CHARACTER_UP 1
#define CHARACTER_RIGHT 2
#define CHARACTER_DOWN 3
#define CHARACTER_LEFT 4

/** Object Class
 */
class Character : public ScrollObject
{
public:
				void			SetKnownRoom(Room *room);
				Room* 			KnownRoom(); 
				void			SetToStartRoomLocation();
				void			JumpToNextRoomLocation(); 
				void			SetFX(const orxSTRING);
				void 			ClearFX();
				void			SetDestinationRoomIndex(int position);
				void			SetDestinationRoomToLastRoom();
//				void			SetDestinationPositionToNextRoomLocation();
				orxVECTOR		GetNextRoomDestinationPosition();
				int				GetDirectionToNextRoom();
				orxBOOL			IsTravelling();
				orxBOOL			IsRotating();
				orxBOOL			IsAtDestinationIndex();
				void			SetTravelling(orxBOOL yesNo);
				void			UpdateTravelPosition(orxVECTOR offsetPosition);

protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);
				virtual orxBOOL OnCollide(ScrollObject *_poCollider,
					orxBODY_PART *_pstPart,
					orxBODY_PART *_pstColliderPart,
					const orxVECTOR &_rvPosition,
					const orxVECTOR &_rvNormal);

private:
				Room *knownRoom;
				int currentPathIndex = 0;
				int destinationIndex = 0;
				orxBOOL isTravelling;
};

#endif // __CHARACTER_H__
