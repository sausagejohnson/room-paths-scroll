#ifndef __ROOM_H__
#define __ROOM_H__

#include "room-paths-scroll.h"
#include "cell.h"
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

class Room 
{
public:
	Room(int cellsAcross, const orxSTRING roomNameFromConfig);
	Cell* CellAt(int index);
	int CellCount();
	//int GetCellSpacing();

	int GetPathStepLength();
	vector<Cell*> GetPath(int step);
	int GetIndexFromCoords(int x, int y);
	void DumpPathData();
	void DumpCellData();
	void SetStartCell(int x, int y);
	void SetEndCell(int x, int y);
	void PerformPathFind();
	void ResetCellTypeToEmpty(CellType type);
	void ResetAllCheckedAndTried();
	void SetTileWidthAndHeight(orxFLOAT tileWidth, orxFLOAT tileHeight); //used for movement and distance for a character
	void SetRoomOffsetForScreen(orxFLOAT offsetX, orxFLOAT offsetY); //used for where to paint the room on screen
	orxVECTOR GetWorldPositionAtPathIndex(int index);
	
	//Get ratio between width and height character can use for moving
	//around the room when the cells are squashed sizes.
	orxFLOAT cellWidthRatio();	
	orxFLOAT cellHeightRatio();
	orxFLOAT GetRoomOffsetX();
	orxFLOAT GetRoomOffsetY();

	
protected:

private:
	void PopulateCellData(const orxSTRING roomNameFromConfig);
	int GetDistanceBetweenTwoCells(Cell *cell1, Cell *cell2);
	int cellCount;
	int cellsAcross;
	//int cellSpacing = 68;
	Cell *cells;
	Cell *startCell;
	Cell *endCell;

	//default, only used for character movement
	orxFLOAT tileWidth, tileHeight = 64;
	orxFLOAT offsetX, offsetY = 0;


	vector< vector<Cell*> > path; //path array of Cell Arrays
				// [ Cell, Cell ]
				// [ Cell ]
				// [ Cell ]
				// [ Cell, Cell ]
				// [ Cell ] etc
	void AddToPath(Cell *cell);
	void RunTests();
	void BuildPath();
	Cell* BacktrackPathStepsBackToLastJunction(); //return junction cell as the next current cell to continue from
	vector<Cell*> GetAdjacentPathableCells(Cell *cell);
	vector<Cell*> GetClosestCellsToDestination(vector<Cell*> cells, Cell *destination);
	Cell* GetCellAtCoord(int x, int y);
	orxBOOL IsCellInMap(Cell *cell);
	void StripUnchosenCells();
};

#endif // __ROOM_H__
