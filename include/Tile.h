
#ifndef __TILE_H__
#define __TILE_H__

#include "room-paths-scroll.h"

/** Object Class
 */
class Tile : public ScrollObject
{
public:
	void SetColour(orxFLOAT r, orxFLOAT g, orxFLOAT b);			
	void SetPos(orxFLOAT x, orxFLOAT y);

protected:

	void            OnCreate();
	void            OnDelete();
	void            Update(const orxCLOCK_INFO &_rstInfo);

private:
};

#endif // __TILE_H__
