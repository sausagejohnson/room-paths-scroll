#ifndef __CELL_H__
#define __CELL_H__

#include "room-paths-scroll.h"
#include "celltypes.h"

class Cell 
{
public:
	//Cell(int x, int y, CellType type);
	Cell();
	int GetXPos(); //0, 1, 2, ...8 etc
	int GetYPos(); //use cellSpacing product for screen position
	orxBOOL HasBeenTried();
	orxBOOL HasBeenChosen();
	bool operator==(Cell *otherCell); //not working for now. Go with equals()
	bool operator <(Cell &otherCell);
	orxBOOL Equals(Cell *otherCell);

	CellType GetCellType();
	void SetXPos(int x);
	void SetYPos(int y);
	void SetCoords(int x, int y);
	void SetType(CellType cType);
	void SetAsTried();
	void ClearAsTried();
	void SetAsChosen(orxBOOL yesNo); //temporary. Can be cleared when backtracked
	int GetDistance();
	void SetDistance(int d);

protected:

private:
	int indexX;
	int indexY;
	CellType cellType;
	orxBOOL tried = orxFALSE;
	orxBOOL chosen = orxFALSE;
	int distance;

 };

#endif // __CELL_H__
