#ifndef __CELLTYPES_H__
#define __CELLTYPES_H__

enum CellType {
	Empty,
	Blocker,
	Door,
	Start,
	End
};

#endif // __CELLTYPES_H__
