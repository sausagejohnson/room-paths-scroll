/**
 * @file room-paths-scroll.h
 * @date 2-Aug-2022
 */

#ifndef __room_paths_scroll_H__
#define __room_paths_scroll_H__

#define __NO_SCROLLED__
#include "Scroll.h"

/** Game Class
 */
class room_paths_scroll : public Scroll<room_paths_scroll>
{
public:
				orxFLOAT tileWidth = 64;
				orxFLOAT tileHeight = 64;
private:

                orxSTATUS       Bootstrap() const;

                void            Update(const orxCLOCK_INFO &_rstInfo);

                orxSTATUS       Init();
                orxSTATUS       Run();
                void            Exit();
                void            BindObjects();
				void 			BuildRoomTiles();
				void 			ColourRoomTiles();
				
				void			CaptureAndCacheTileSize();

				int shuffleTestIndex = 0;
private:
};

#endif // __room_paths_scroll_H__
