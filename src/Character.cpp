#include "Character.h"

orxBOOL orxVector_ArePrettyMuchEqual(const orxVECTOR *a, const orxVECTOR *b){
	return (orxMath_Abs(a->fX - b->fX) < 0.5) && (orxMath_Abs(a->fY - b->fY) < 0.5);
}

void Character::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
}

void Character::SetKnownRoom(Room *room){
	knownRoom = room;
	//SetToStartRoomLocation();
}

Room* Character::KnownRoom(){
	return knownRoom;
}

void Character::SetToStartRoomLocation(){
	orxVECTOR worldPosition = knownRoom->GetWorldPositionAtPathIndex(0);
	orxVECTOR currentPosition = {};
	this->GetPosition(currentPosition, orxTRUE);
	
	worldPosition.fZ = currentPosition.fZ;
	
	this->SetPosition(worldPosition);
	currentPathIndex = 0;
}

void Character::SetFX(const orxSTRING fxName){
	orxOBJECT *orxObject = this->GetOrxObject();
	//orxObject_RemoveAllFXs(orxObject);
	ClearFX();
	orxObject_AddUniqueFX(orxObject, fxName);
}

void Character::ClearFX(){
	orxOBJECT *orxObject = this->GetOrxObject();
	orxObject_RemoveAllFXs(orxObject);
}

/*
 * Sets the index position of the map path that the character will travel to, and stop at.
 * */
void Character::SetDestinationRoomIndex(int index){
	destinationIndex = index;
}

/*
 * The normal situation. Most characters need to travel to the position of the full 
 * found map path.
 * */
void Character::SetDestinationRoomToLastRoom(){
	destinationIndex = knownRoom->GetPathStepLength()-1;
}

orxVECTOR Character::GetNextRoomDestinationPosition(){
	int nextPathIndex = currentPathIndex + ((currentPathIndex == destinationIndex) ? 0 : 1);
	if (nextPathIndex >= knownRoom->GetPathStepLength())
		nextPathIndex = 0;
	
	orxVECTOR worldPosition = knownRoom->GetWorldPositionAtPathIndex(nextPathIndex);
	orxVECTOR currentPosition = {};
	this->GetPosition(currentPosition, orxTRUE);
	
	worldPosition.fZ = currentPosition.fZ;
	
	return worldPosition;
}

orxBOOL Character::IsAtDestinationIndex(){
	return currentPathIndex == destinationIndex;
}

orxBOOL Character::IsTravelling(){
	return isTravelling;
}

void Character::SetTravelling(orxBOOL yesNo){
	isTravelling = yesNo;
}

int	Character::GetDirectionToNextRoom(){
	orxVECTOR current = {};
	this->GetPosition(current, orxTRUE);
	//orxVector_Normalize(&current, &current);

	orxVECTOR destination = {};
	destination = this->GetNextRoomDestinationPosition();
	//orxVector_Normalize(&destination, &destination);

	//orxLOG("curr x:%f y:%f, dest x:%f y:%f", current.fX, current.fY, destination.fX, destination.fY);

	if (current.fX >= 200) {
		int here = 1;
	}

	if (destination.fX == current.fX){
		if (destination.fY < current.fY){
			return CHARACTER_UP;
		} else if (destination.fY > current.fY){
			return CHARACTER_DOWN;
		}
	}

	if (destination.fY == current.fY){
		if (destination.fX < current.fX){
			return CHARACTER_LEFT;
		} else if (destination.fX > current.fX){
			return CHARACTER_RIGHT;
		}
	}

	return orxNULL;

}

void Character::UpdateTravelPosition(orxVECTOR offsetPosition){
	orxVECTOR current = {};
	orxVECTOR nextPosition = {};
	
	this->GetPosition(current, orxTRUE); //get world
	
	orxVector_Add(&nextPosition, &current, &offsetPosition);
	this->SetPosition(nextPosition, orxTRUE); //set world
	
	int direction = this->GetDirectionToNextRoom();
	if (direction == CHARACTER_RIGHT){
		this->SetRotation(orxMATH_KF_DEG_TO_RAD * 0, orxFALSE);
	}
	if (direction == CHARACTER_DOWN){
		this->SetRotation(orxMATH_KF_DEG_TO_RAD * 90, orxFALSE);
	}
	if (direction == CHARACTER_LEFT){
		this->SetRotation(orxMATH_KF_DEG_TO_RAD * 180, orxFALSE);
	}
	if (direction == CHARACTER_UP){
		this->SetRotation(orxMATH_KF_DEG_TO_RAD * 270, orxFALSE);
	}
	
}


void Character::JumpToNextRoomLocation(){
	if (currentPathIndex == destinationIndex){
		return;
	}
	currentPathIndex++;
	if (currentPathIndex >= destinationIndex){
		//you're there. Don't allow any more travel
		this->SetTravelling(orxFALSE);
		return;
	}
	
	if (currentPathIndex >= knownRoom->GetPathStepLength())
		currentPathIndex = 0;
	
	orxVECTOR worldPosition = knownRoom->GetWorldPositionAtPathIndex(currentPathIndex);
	orxVECTOR currentPosition = {};
	this->GetPosition(currentPosition, orxTRUE);
	
	worldPosition.fZ = currentPosition.fZ;
	
	this->SetPosition(worldPosition);
}



void Character::OnDelete()
{
}

void Character::Update(const orxCLOCK_INFO &_rstInfo)
{

	if (this->IsTravelling()){
		orxVECTOR directionSegment = {};
		orxVECTOR current = {};
		this->GetPosition(current, orxTRUE);

		orxVECTOR destination = {};
		destination = this->GetNextRoomDestinationPosition();

		orxRGBA gridColour;
		gridColour.u8R = 255;
		gridColour.u8G = 90;
		gridColour.u8B = 90;
		gridColour.u8A = 255;
		orxVECTOR screenCurrent, screenDestination;
		orxRender_GetScreenPosition(&current, orxNULL, &screenCurrent);
		orxRender_GetScreenPosition(&destination, orxNULL, &screenDestination);

		orxDisplay_DrawLine ( &screenCurrent, &screenDestination, gridColour);
		
		if (orxVector_ArePrettyMuchEqual(&destination, &current)){
			orxLOG("SAME! %f %f", current.fX, current.fY);
			if (this->IsAtDestinationIndex()){
				this->SetTravelling(orxFALSE);
			}
			this->JumpToNextRoomLocation();
			return;
		} else {
			orxVector_Sub(&directionSegment, &destination, &current);
			orxVECTOR normalisedDirection = {};
			
			orxVECTOR normalised = {};
			orxVector_Normalize(&normalised, &directionSegment);
			
			//add some movement scaling if the tile is not square. 
			orxVECTOR normalizedStretch = {knownRoom->cellWidthRatio(), knownRoom->cellHeightRatio()};
			orxVector_Mul(&normalised, &normalised, &normalizedStretch);
			
			orxFLOAT distanceToDestination = orxVector_GetDistance(&current, &destination);
			orxLOG("distanceToDestination: %f", distanceToDestination);
			//orxLOG("from: %f,%f to %f,%f distance: %f", screenCurrent.fX, screenCurrent.fY, screenDestination.fX, screenDestination.fY, distanceToDestination);
			//orxLOG("from: %f,%f to %f,%f distance: %f", current.fX, current.fY, destination.fX, destination.fY, distanceToDestination);
			
			this->UpdateTravelPosition(normalised);
		}
		
	}

}

orxBOOL Character::OnCollide(ScrollObject *_poCollider,
	orxBODY_PART *_pstPart,
	orxBODY_PART *_pstColliderPart,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	if (_poCollider == orxNULL) {
		return orxTRUE;
	}

	const orxSTRING colliderName = _poCollider->GetModelName();
	if (orxString_SearchString(colliderName, "Shield") != orxNULL) {
		
	}

	return orxTRUE;
}
