#include "Room.h"

Room::Room(int numcellsAcross, const orxSTRING roomNameFromConfig){
	cellsAcross = numcellsAcross;
	cellCount = cellsAcross*cellsAcross;
	cells = new Cell[cellCount];

	PopulateCellData(roomNameFromConfig);
	
}

/*
 * Perform the path finding work.
 * The resulting path lives in the path variable.
 * */
void Room::PerformPathFind(){
	BuildPath();
	StripUnchosenCells();
//	DumpPathData();
//	DumpCellData();

	RunTests();	
}

/*
 * Build up the pathing data in a rough form.
 * Nodes require stripping for a clean path.
 * */
void Room::BuildPath(){
	path.clear();
	
	Cell *currentCell = startCell;
	currentCell->SetAsChosen(orxTRUE);
	
	vector<Cell*> singleCellInStep;
	singleCellInStep.push_back(currentCell);
	path.push_back(singleCellInStep);
	
	orxBOOL finishedPath = orxFALSE;
	
	while (finishedPath == orxFALSE){
		vector<Cell*> adjacentCells = GetAdjacentPathableCells(currentCell);
		vector<Cell*> closestCells = GetClosestCellsToDestination(adjacentCells, endCell);
		//orxLOG("adjacentC %p: size: %d", (void*)&adjacentCells, adjacentCells.size());
		//orxLOG("closestCe %p: size: %d", (void*)&closestCells, closestCells.size());
		if (closestCells.size() == 0){
			
			currentCell = BacktrackPathStepsBackToLastJunction();
			if (currentCell == orxNULL){ //can't find any more junctions to try
				finishedPath = orxTRUE;
			}

		} else {
			path.push_back(closestCells);
		}
		
		if (closestCells.size() >= 1){
			currentCell = closestCells[0]; //could be random choice too maybe?
			currentCell->SetAsChosen(orxTRUE);
			if (currentCell->Equals(endCell)){
				finishedPath = orxTRUE;
			}
			
			if (closestCells.size() > 1){
				currentCell->SetAsTried();
			}
		}
	}
	
}

void Room::SetTileWidthAndHeight(orxFLOAT tileW, orxFLOAT tileH){
	tileWidth = tileW;
	tileHeight = tileH;
}

void Room::SetRoomOffsetForScreen(orxFLOAT x, orxFLOAT y){
	offsetX = x;
	offsetY = y;
}

orxFLOAT Room::GetRoomOffsetX(){
	return offsetX;
}
orxFLOAT Room::GetRoomOffsetY(){
	return offsetY;
}

orxFLOAT Room::cellWidthRatio(){
	if (tileWidth <= tileHeight) return 1;
	return tileWidth / tileHeight;
}

orxFLOAT Room::cellHeightRatio(){
	if (tileHeight <= tileWidth) return 1;
	return tileHeight / tileWidth;
}

/*
 * Strip the unused nodes off a dirty path to produce
 * a single clean path from start to end.
 * */
void Room::StripUnchosenCells(){
	int pathSize = path.size();
	for (int i=0; i<pathSize; i++){
		vector<Cell*> step = path[i];
			
		for (int j=0; j<step.size(); j++){
			int stepSizeTest = step.size();
			Cell *cellFromStep = step[j];
			if (!cellFromStep->HasBeenChosen()){
				cellFromStep->ClearAsTried();
				step.erase(step.begin()+j);
				j=0; //start over
			}
		}
		path[i] = step;
	}
	
	//Now clear any cell flags not in the path map
	for (int i=0; i<cellCount; i++){
		Cell* cell = &cells[i];
		if (!IsCellInMap(cell) && cell->GetCellType() == CellType::Empty){
			cell->ClearAsTried();
			cell->SetAsChosen(orxFALSE);
		}
	}
}


/*
 * Log out the entire cell array and current properties for debugging.
 * */
void Room::DumpCellData(){

	for (int i=0; i<cellCount; i++){
		Cell* cell = &cells[i];
		cout << "Cell: " << i << " Type: " << cell->GetCellType();
		cout << " (" << cell->GetXPos() << "," << cell->GetYPos() << ") ";
		if (cell->HasBeenChosen()) cout << "C";
		if (cell->HasBeenTried()) cout << "T";
		cout << endl;
	}
	cout << endl;
}

/*
 * Log out the path data for debugging.
 * */
void Room::DumpPathData(){
	int pathSize = path.size();
	for (int i=0; i<pathSize; i++){
		vector<Cell*> step = path[i];
		cout << "Step: " << i << " ";
			
		for (int j=0; j<step.size(); j++){
			Cell *cellFromStep = step[j];
			cout << "(" << cellFromStep->GetXPos() << ", " << cellFromStep->GetYPos() << ")";
			if (cellFromStep->HasBeenChosen())
				cout << "C";
			if (cellFromStep->HasBeenTried())
				cout << "T";
			cout << " ";
		}
		
		cout << endl;

	}
	cout << endl;
}


/*
 * Used by the path builder. When a chosen direction runs dry,
 * backtrack to the last junction to try another direction.
 * */
Cell* Room::BacktrackPathStepsBackToLastJunction(){
	
	int pathSize = path.size();
	vector<Cell*> endStep = path[pathSize-1];

	//search backwards for last junction
	for (int i=path.size()-1; i>0; i--){

		vector<Cell*> step = path[i];
		int junctionSize = step.size();
		if (junctionSize > 1){ //is a junction, multiple direction choices
			for (int j=0; j<junctionSize; j++){
				
				Cell *junctionCell = step[j];
				if (junctionCell->GetXPos() == 4 && junctionCell->GetYPos() == 0){
					int stophere = 1;
				}
				if (junctionCell->HasBeenChosen()){//was a failure, so unchoose it
					junctionCell->SetAsChosen(orxFALSE); 
				}
				if (!junctionCell->HasBeenTried()){ //not tried? Mark it, return it, for attempting
					junctionCell->SetAsTried(); //dirty it and send it back to travelling along
					junctionCell->SetAsChosen(orxTRUE); 
					return junctionCell;
				}
				
			}
		} else { //single unit path steps that are failures. Delete as you go. Don't want it anymore.
			
		}
		Cell *c = step[0];
		path.erase(path.end()-1);
	}

	return orxNULL;
}

Cell* Room::GetCellAtCoord(int x, int y){
	if (x < 0 || x >= cellsAcross)
		return orxNULL;

	if (y < 0 || y >= cellsAcross)
		return orxNULL;

	int index = GetIndexFromCoords(x, y);
	
	return &cells[index];
}

int Room::GetPathStepLength(){
	return path.size();
}

int Room::GetIndexFromCoords(int x, int y){
	return (y * cellsAcross) + x;
}

vector<Cell*> Room::GetPath(int step){
	return path[step];
}

/* 
 * Pass a vector of Cells, and find one or more that are the closest
 * to the destination/end cell.
 */
vector<Cell*> Room::GetClosestCellsToDestination(vector<Cell*> adjacentCells, Cell *destination){
	//orxLOG("Before sorting:");
	for (int i=0; i<adjacentCells.size(); i++){
		Cell *cell = adjacentCells[i];
		int distance = GetDistanceBetweenTwoCells(cell, destination);
		cell->SetDistance(distance);
		//orxLOG("cell %d", cell->GetDistance());
	}
	
	//zero or one cell, return that. No point calculating.
	if (adjacentCells.size() <= 1){
		return adjacentCells;
	}

	for (int i=0; i<adjacentCells.size(); i++){
		Cell *cell = adjacentCells[i];
		if (i+1 < adjacentCells.size()){
			Cell *nextCell = adjacentCells[i+1];
			if (cell->GetDistance() > nextCell->GetDistance()){
				adjacentCells.erase(adjacentCells.begin()+i+1);
				adjacentCells.insert(adjacentCells.begin(), nextCell);
				//swap occured. Start over.
				i=0;
			} else {
				//do nothing.move on
			}
		}
	
	}
	
	return adjacentCells;
}


orxBOOL Room::IsCellInMap(Cell *cell){
	//orxBOOL result = orxFALSE;
	
	int pathStepLength = this->GetPathStepLength();
	
	for (int i=0; i<pathStepLength; i++){
		vector<Cell*> stepCells = this->GetPath(i);
		int choiceCellsLength = stepCells.size();
		for (int j=0; j<choiceCellsLength; j++){
			Cell *pathCell = stepCells[j];
			if (pathCell->GetXPos() == cell->GetXPos() && pathCell->GetYPos() == cell->GetYPos()){
				return orxTRUE;
			}
		}
	}
	
	return orxFALSE;
}

/*
 * Get the cell to the north, south, east and west if the specified cell.
 * But don't include any cells already in the path, or any cell
 * that is not an empty cell.
 * */
vector<Cell*> Room::GetAdjacentPathableCells(Cell *cell){
	
	vector<Cell*> pathableCells;
	int x,y;
	
	//get top cell from the current cell
	y = cell->GetYPos()-1;
	x = cell->GetXPos();
	if (y >= 0){
		Cell *cellAbove = GetCellAtCoord(x, y);
		if (cellAbove->GetCellType() != CellType::Blocker && !IsCellInMap(cellAbove)){
			pathableCells.push_back(cellAbove);
			//orxLOG("Above: %d, %d", x, y);
		}
	}

	//get bottom cell from the current cell
	y = cell->GetYPos()+1;
	x = cell->GetXPos();
	if (y < cellsAcross){
		Cell *cellBelow = GetCellAtCoord(x, y);
		if (cellBelow->GetCellType() != CellType::Blocker && !IsCellInMap(cellBelow)){
			pathableCells.push_back(cellBelow);
			//orxLOG("Below: %d, %d", x, y);
		}
	}

	//get left cell from the current cell
	y = cell->GetYPos();
	x = cell->GetXPos()-1;
	if (x >= 0){
		Cell *cellLeft = GetCellAtCoord(x, y);
		if (cellLeft->GetCellType() != CellType::Blocker && !IsCellInMap(cellLeft)){
			pathableCells.push_back(cellLeft);
			//orxLOG("Left: %d, %d", x, y);
		}
	}

	//get right cell from the current cell
	y = cell->GetYPos();
	x = cell->GetXPos()+1;
	if (x < cellsAcross){
		Cell *cellRight = GetCellAtCoord(x, y);
		if (cellRight->GetCellType() != CellType::Blocker && !IsCellInMap(cellRight)){
			pathableCells.push_back(cellRight);
			//orxLOG("Right: %d, %d", x, y);
		}
	}
	
	return pathableCells;
	
}

/*
 * Total amount of cells in the room.
 * */
int Room::CellCount(){
	return cellCount;
}

/*
 * Marked to remove. For visual display, so not appropriate for this class.
 * */
//int Room::GetCellSpacing(){
//	return cellSpacing;
//}

Cell* Room::CellAt(int index){
	return &cells[index];
}

orxVECTOR Room::GetWorldPositionAtPathIndex(int index){
	vector<Cell*> pathCells = this->GetPath(index);
	orxASSERT(pathCells.size() == 1); //if cleaned, should only contain one. Clean first if so.
	
	Cell* pathCell = pathCells[0];
	
	orxVECTOR worldPosition = { (orxFLOAT)((pathCell->GetXPos() * tileWidth) + GetRoomOffsetX()), 
								(orxFLOAT)((pathCell->GetYPos() * tileHeight) + GetRoomOffsetY())
								};
	
	return worldPosition;
}


int Room::GetDistanceBetweenTwoCells(Cell *cell1, Cell *cell2){
	int distance = 0;
	
	int horizontalBlockDistance = orxMath_Abs( cell1->GetXPos() - cell2->GetXPos() );
	int verticalBlockDistance = orxMath_Abs( cell1->GetYPos() - cell2->GetYPos() );
	
	int shortestBlockDistance = (horizontalBlockDistance < verticalBlockDistance) ? horizontalBlockDistance : verticalBlockDistance;
	
	int remainingBlockDistance = orxMath_Abs( horizontalBlockDistance - verticalBlockDistance );
	
	distance = (shortestBlockDistance * 14) + (remainingBlockDistance * 10);
	
	return distance;
}

void Room::AddToPath(Cell *cell){

	vector<Cell*> cells;
    cells.push_back( cell );
	
	path.push_back(cells);
}

/*
 * Set all the cells of a type to an empty type. 
 * Used to reset rooms for re-pathing. 
 * */
void Room::ResetCellTypeToEmpty(CellType type){
	for (int i=0; i<cellCount; i++){
		Cell* cell = &cells[i];
		if (cell->GetCellType() == type){
			cell->SetType(CellType::Empty);
		}
	}
}

/*
 * Clear all the cells Chosen ad Tried properties. 
 * Used to reset rooms for re-pathing. 
 * */
void Room::ResetAllCheckedAndTried(){
	for (int i=0; i<cellCount; i++){
		Cell* cell = &cells[i];
		cell->SetAsChosen(orxFALSE);
		cell->ClearAsTried();
	}
}

/*
 * Reset which cell is the start cell. Throws an error if cell is not a blank cell.
 * */
void Room::SetStartCell(int x, int y){
	ResetCellTypeToEmpty(CellType::Start);
	ResetAllCheckedAndTried();
	
	startCell = GetCellAtCoord(x, y);
	if (startCell->GetCellType() == CellType::Empty){
		startCell->SetType(CellType::Start);	
	} else {
		orxASSERT(startCell->GetCellType() == CellType::Empty);
		//throw "Can only set StartIndex for pathfinding on an Empty Cell";
	}
	
}

/*
 * Reset which cell is the destination cell. Throws an error if cell is not a blank cell.
 * */
void Room::SetEndCell(int x, int y){
	ResetCellTypeToEmpty(CellType::End);
	ResetAllCheckedAndTried();
	
	endCell = GetCellAtCoord(x, y);
	if (endCell->GetCellType() == CellType::Empty){
		endCell->SetType(CellType::End);	
	} else {
		orxASSERT(endCell->GetCellType() == CellType::Empty);
		//throw "Can only set StartIndex for pathfinding on an Empty Cell";
	}
}

/*
 * Load in a room map from the config file.
 * Pass in a string, eg. Room2.
 * */
void Room::PopulateCellData(const orxSTRING roomNameFromConfig){
	if (orxConfig_PushSection(roomNameFromConfig)){
		for (int i=0; i<orxConfig_GetListCount("Cells"); i++){
			const orxSTRING cellName = orxConfig_GetListString("Cells", i);
			cells[i].SetXPos( (orxMath_Mod(i, cellsAcross)) ); //*cellSpacing
			cells[i].SetYPos( (i/(int)cellsAcross) ); //*cellSpacing
			
			if (orxString_Compare(cellName, "X") == 0){
				cells[i].SetType(CellType::Blocker);
			} else 	if (orxString_Compare(cellName, "O") == 0){
				cells[i].SetType(CellType::Empty);
			} else {
				cells[i].SetType(CellType::Empty);
			}
		}
		
		orxConfig_PopSection();
	}
}

/*
 * Distance tests.
 * */
void Room::RunTests(){
	orxLOG("Running Room Tests");
	orxLOG("==================");

	//Test 1
	Cell *cell1 = new Cell();
	cell1->SetCoords(1, 5);	
	Cell *cell2 = new Cell();
	cell2->SetCoords(6, 1);	
	int distance = GetDistanceBetweenTwoCells(cell1, cell2);
	orxLOG("distance: %d", distance);
	orxASSERT(distance == 66);

	//Test 2
	cell1->SetCoords(0, 0);	
	cell2->SetCoords(2, 7);	
	distance = GetDistanceBetweenTwoCells(cell1, cell2);
	orxLOG("distance: %d", distance);
	orxASSERT(distance == 78);
	
	//Test 3
	cell1->SetCoords(3, 3);	
	cell2->SetCoords(4, 3);	
	distance = GetDistanceBetweenTwoCells(cell1, cell2);
	orxLOG("distance: %d", distance);
	orxASSERT(distance == 10);
	
	//Test 4
	cell1->SetCoords(5, 5);	
	cell2->SetCoords(6, 6);	
	distance = GetDistanceBetweenTwoCells(cell1, cell2);
	orxLOG("distance: %d", distance);
	orxASSERT(distance == 14);
	
	cell1->SetCoords(1, 1);	
	cell2->SetCoords(1, 2);
	orxASSERT(!cell1->Equals(cell2));
//	if (cell1 != cell2){
//		orxLOG("not equal. Good");
//	} else {
//		orxLOG("bad");
//	}

	cell1->SetCoords(1, 1);	
	cell2->SetCoords(1, 1);
	orxASSERT(cell1->Equals(cell2));
//	if (cell1 == cell2){
//		orxLOG("equal, good");
//	} else {
//		orxLOG("bad");
//	}

}