/**
 * @file Object.cpp
 * @date 2-Aug-2022
 */

#include "Tile.h"

void Tile::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
}

void Tile::SetPos(orxFLOAT x, orxFLOAT y){
	orxVECTOR position = {x, y, 0};
	this->SetPosition(position);
	
	
}

void Tile::SetColour(orxFLOAT r, orxFLOAT g, orxFLOAT b){
	orxVECTOR vec;
	vec.fX = r; 
	vec.fY = g; 
	vec.fZ = b; 
 
	orxCOLOR colour;
	colour.vRGB = vec;
	colour.fAlpha = orxFLOAT_1; 
 
	this->SetColor(colour);
}

void Tile::OnDelete()
{
}

void Tile::Update(const orxCLOCK_INFO &_rstInfo)
{
}
