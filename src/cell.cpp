#include "cell.h"

Cell::Cell() 
{
	indexX = 1,
	indexY = 1;
}

int Cell::GetXPos(){
	return indexX; 
}

int Cell::GetYPos(){
	return indexY; 
}

CellType Cell::GetCellType(){
	return cellType; 
}

orxBOOL Cell::HasBeenTried(){
	return tried;
}

void Cell::SetAsTried(){
	tried = orxTRUE;
}

void Cell::ClearAsTried(){
	tried = orxFALSE;
}

orxBOOL Cell::HasBeenChosen(){
	return chosen;
}

void Cell::SetAsChosen(orxBOOL yesNo){
	chosen = yesNo;
}

void Cell::SetXPos(int x){
	indexX = x;
}

void Cell::SetYPos(int y){
	indexY = y;
}

void Cell::SetCoords(int x, int y){
	indexX = x;
	indexY = y;
}

void Cell::SetType(CellType cType){
	cellType = cType;
}

int Cell::GetDistance(){
	return distance;
}

void Cell::SetDistance(int d){
	distance = d;
}

/* These operator overloads don't work. Replace code with them when fixed. */
bool Cell::operator==(Cell *otherCell) {
	return (this->GetXPos() == otherCell->GetXPos() && this->GetYPos() == otherCell->GetYPos());
}

bool Cell::operator <(Cell &otherCell) {
    return GetDistance() < otherCell.GetDistance();
}

/* This is used instead of the above operators for now. */
orxBOOL Cell::Equals(Cell *otherCell){
	return (this->GetXPos() == otherCell->GetXPos() && this->GetYPos() == otherCell->GetYPos());
}


