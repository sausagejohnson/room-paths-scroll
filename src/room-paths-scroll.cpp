/**
 * @file room-paths-scroll.cpp
 * @date 2-Aug-2022
 */

#define __SCROLL_IMPL__
#include "room-paths-scroll.h"
#undef __SCROLL_IMPL__

#include "Tile.h"
#include "Character.h"
#include "cell.h"
#include "Room.h"
#include <vector>

vector<Tile*> tileObjects;
Room *room; 
Character *character;

#ifdef __orxMSVC__

/* Requesting high performance dedicated GPU on hybrid laptops */
__declspec(dllexport) unsigned long NvOptimusEnablement        = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;

#endif // __orxMSVC__

/** Update function, it has been registered to be called every tick of the core clock
 */
void room_paths_scroll::Update(const orxCLOCK_INFO &_rstInfo)
{
	
	//Press 1 Key for a specfic start, end and path
	if (orxInput_HasBeenActivated("Test1")){
		room->SetStartCell(2, 7);
		room->SetEndCell(7, 0);
		room->PerformPathFind();

		ColourRoomTiles();
		character->SetKnownRoom(room);
	}
	
	//Press 2 Key for a specfic start, end and path
	if (orxInput_HasBeenActivated("Test2")){
		room->SetStartCell(1,1);
		room->SetEndCell(7, 0);
		room->PerformPathFind();
		ColourRoomTiles();
		character->SetKnownRoom(room);

	}
	
	//Press space to shuffle the start position to the next empty cell.
	//This helps test every starting cell position.
	if (orxInput_HasBeenActivated("ShuffleTest")){
		shuffleTestIndex++;
		if (shuffleTestIndex >= room->CellCount()) shuffleTestIndex = 0;

		orxBOOL foundNextEmpty = orxFALSE;
		Cell *c = orxNULL;
		
		while (foundNextEmpty == orxFALSE){
				
			c = room->CellAt(shuffleTestIndex);
			if (!c->GetCellType() == CellType::Empty){
				shuffleTestIndex++;
				if (shuffleTestIndex > room->CellCount()) shuffleTestIndex = 0;
			} else {
				foundNextEmpty = orxTRUE;
			}
		}
		
		if (c != orxNULL){
			room->SetStartCell(c->GetXPos(), c->GetYPos() );
			room->SetEndCell(0, 7);
			room->PerformPathFind();
			
			ColourRoomTiles();
			character->SetKnownRoom(room);
		}
		
	}
	
	if (orxInput_HasBeenActivated("JumpAlongPath"))
    {
		character->JumpToNextRoomLocation();
    }

	if (orxInput_HasBeenActivated("SlideAlongPathToDestination"))
    {
		//character->SetDestinationRoomIndex(5);
		character->SetDestinationRoomToLastRoom();
		character->SetTravelling(orxTRUE);
    }
	
	if (orxInput_HasBeenActivated("RemoveMoveFX"))
    {
		
    }
	
    // Should quit?
    if(orxInput_IsActive("Quit"))
    {
        // Send close event
        orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
    }
}

/*
 * Create a grid of tiles for visualising the status of the cells
 * and the path.
 * */
void room_paths_scroll::BuildRoomTiles(){
	for (int i=0; i<room->CellCount(); i++){
		Cell *cell = room->CellAt(i);
		const CellType cellType = cell->GetCellType();
		Tile *tile = (Tile*)CreateObject("Tile");
		tile->SetPos( (cell->GetXPos() * tileWidth) + room->GetRoomOffsetX(), (cell->GetYPos() * tileHeight) + room->GetRoomOffsetY() );
		
		tileObjects.push_back(tile);
	}
}

/*
 * Colour the tile grid using the properties in the array of cells
 * stored in the room object. 
 * */
void room_paths_scroll::ColourRoomTiles(){
	int cellCount = room->CellCount();
	
	for (int i=0; i<cellCount; i++){
		Tile *tile = tileObjects[i];
		
		Cell *cell = room->CellAt(i);
		CellType cellType = cell->GetCellType();
			
		if (cellType == CellType::Empty){
			tile->SetColour(1.0, 1.0, 1.0);		
			if (cell->HasBeenTried() ){
				tile->SetColour(0.7, 0.5, 0.25);	
			} 
			if (cell->HasBeenChosen() ){
				tile->SetColour(0.25, 1.0, 0.25);
			}
			
		}
		if (cellType == CellType::Start){
			tile->SetColour(0.0, 0.7, 0);
		}
		if (cellType == CellType::End){
			tile->SetColour(1.0, 0.2, 0);
		}
		if (cellType == CellType::Blocker){
			tile->SetColour(0.25, 0.25, 0.25);
		}
		

	}
}


void room_paths_scroll::CaptureAndCacheTileSize(){
	if (orxConfig_PushSection("Tile")){
		const orxSTRING tileFileName = orxConfig_GetString("Texture");
		
		orxLOG("tileFilename %s", tileFileName);
		
		orxTEXTURE *tileTexture = orxTexture_Load(tileFileName, orxFALSE);
		orxTexture_GetSize(tileTexture, &tileWidth, &tileHeight);
		orxConfig_PopSection();
	}
	
}

orxSTATUS room_paths_scroll::Init()
{
	CaptureAndCacheTileSize();
	
	orxFLOAT cellsAcross = 8;
	
	//First default room and path
	room = new Room(cellsAcross, "Room3");
	room->SetTileWidthAndHeight(tileWidth, tileHeight);
	room->SetRoomOffsetForScreen(-400, -300);
	room->SetStartCell(1, 0);
	room->SetEndCell(7, 7);
	room->PerformPathFind();

	BuildRoomTiles();
	ColourRoomTiles();

	character = (Character*)CreateObject("Character");
	character->SetKnownRoom(room);
	character->SetToStartRoomLocation();
	//character->MoveToNextRoomLocation();

	orxLOG("c: %d", character->KnownRoom()->CellCount());

    return orxSTATUS_SUCCESS;
}

/** Run function, it should not contain any game logic
 */
orxSTATUS room_paths_scroll::Run()
{
    // Return orxSTATUS_FAILURE to instruct orx to quit
    return orxSTATUS_SUCCESS;
}

/** Exit function, it is called before exiting from orx
 */
void room_paths_scroll::Exit()
{
    // Let orx clean all our mess automatically. :)
}

/** BindObjects function, ScrollObject-derived classes are bound to config sections here
 */
void room_paths_scroll::BindObjects()
{
    // Bind the Object class to the Object config section
    ScrollBindObject<Tile>("Tile");
    ScrollBindObject<Character>("Character");
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS room_paths_scroll::Bootstrap() const
{
    // Add config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Execute our game
    room_paths_scroll::GetInstance().Execute(argc, argv);

    // Done!
    return EXIT_SUCCESS;
}
